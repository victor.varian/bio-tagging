import spacy
import pickle

from spacy.language import Language
from spacy.tokens import Doc

class WhitespaceTokenizer(object):
    def __init__(self, vocab):
        self.vocab = vocab

    def __call__(self, text):
        words = text.split(' ')
        # All tokens 'own' a subsequent space character in this tokenizer
        spaces = [True] * len(words)
        return Doc(self.vocab, words=words, spaces=spaces)

    def to_disk(self, path, **kwargs):
        # This will receive the directory path + /my_component
        with open(path, 'wb') as handle:
            handle.write(pickle.dumps(self.__dict__))

    def from_disk(self, path, **kwargs):
        with open(path, 'rb') as handle:
            self.__dict__.update(pickle.loads(handle.read()))


Language.Defaults.create_tokenizer = WhitespaceTokenizer
output_dir='../spacy_out'
print("Loading from", output_dir)
nlp2 = spacy.load(output_dir)

def create_tokenizer(nlp):
    return WhitespaceTokenizer(nlp)  # or however you custom tokenizer is initialised

text = input()
while (text != ""):
    doc2 = nlp2(text)
    for ent in doc2.ents:
        print(ent.text, ent.label_)

    print()
    text = input()

"""Example of training an additional entity type

This script shows how to add a new entity type to an existing pretrained NER
model. To keep the example short and simple, only four sentences are provided
as examples. In practice, you'll need many more — a few hundred would be a
good start. You will also likely need to mix in examples of other entity
types, which might be obtained by running the entity recognizer over unlabelled
sentences, and adding their annotations to the training set.

The actual training is performed by looping over the examples, and calling
`nlp.entity.update()`. The `update()` method steps through the words of the
input. At each word, it makes a prediction. It then consults the annotations
provided on the GoldParse instance, to see whether it was right. If it was
wrong, it adjusts its weights so that the correct action will score higher
next time.

After training your model, you can save it to a directory. We recommend
wrapping models as Python packages, for ease of deployment.

For more details, see the documentation:
* Training: https://spacy.io/usage/training
* NER: https://spacy.io/usage/linguistic-features#named-entities

Compatible with: spaCy v2.1.0+
Last tested with: v2.2.4
"""
from __future__ import unicode_literals, print_function

import plac
import random
import warnings
from pathlib import Path
from spacy.language import Language
from spacy.tokens import Doc
import spacy
import json
import pickle
import pandas as pd
from spacy.util import minibatch, compounding
from sklearn.metrics import classification_report

# new entity label
B = "B"
I = "I"
O = "O"

# training data
# Note: If you're using an existing model, make sure to mix in examples of
# other entity types that spaCy correctly recognized before. Otherwise, your
# model might learn the new type, but "forget" what it previously knew.
# https://explosion.ai/blog/pseudo-rehearsal-catastrophic-forgetting

df = pd.read_csv('../input/tag_dataset.csv', header=None, encoding="latin-1")
df = df[:202]
max_len = len(df.columns)

texts = df[::2]
texts.reset_index(inplace=True)
del texts['index']

labels = df[1::2]
labels.reset_index(inplace=True)
del labels['index']

num_to_tag = {
    2: "O",
    1: "I",
    0: "B"
}

train_sentences = []
with open('../input/train.pkl', 'rb') as handle:
    tmp_train = pickle.load(handle)

    for train_ex in tmp_train:
        train_sentences.append(" ".join(train_ex))


test_sentences = []
with open('../input/test.pkl', 'rb') as handle:
    tmp_test = pickle.load(handle)

    for test_ex in tmp_test:
        test_sentences.append(" ".join(test_ex))


train_tag = []
with open('../input/train_tag.pkl', 'rb') as handle:
    tmp_train = pickle.load(handle)
    for tag_list in tmp_train:
        tmp_label = []
        
        for num in tag_list:
            tmp_label.append(num_to_tag[num])
        
        train_tag.append(tmp_label)

test_tag = []
with open('../input/test_tag.pkl', 'rb') as handle:
    tmp_test = pickle.load(handle)
    for tag_list in tmp_test:
        tmp_label = []
        
        for num in tag_list:
            tmp_label.append(num_to_tag[num])
        
        test_tag.append(tmp_label)


entities = []

for i in range(len(train_sentences)):
    sentence = train_sentences[i]
    tags = train_tag[i]
    
    entity = []
    tag_counter = 0
    tmp_tuple = [0, None, None]

    for i in range(len(sentence)):
        if (sentence[i] == " "):
            tmp_tuple[1] = i
            tmp_tuple[2] = tags[tag_counter]
            entity.append(tuple(tmp_tuple))
            tmp_tuple = [i+1, None, None]
            tag_counter += 1

    tmp_tuple[1] = i+1
    tmp_tuple[2] = tags[tag_counter]
    entity.append(tuple(tmp_tuple))
    entities.append(entity)



TRAIN_DATA = []
for i in range(len(train_sentences)):
    tmp_tuple = (train_sentences[i], {"entities": entities[i]})
    TRAIN_DATA.append(tmp_tuple)


class WhitespaceTokenizer(object):
    def __init__(self, vocab):
        self.vocab = vocab

    def __call__(self, text):
        words = text.split(' ')
        # All tokens 'own' a subsequent space character in this tokenizer
        spaces = [True] * len(words)
        return Doc(self.vocab, words=words, spaces=spaces)

    def to_disk(self, path, **kwargs):
        # This will receive the directory path + /my_component
        with open(path, 'wb') as handle:
            handle.write(pickle.dumps(self.__dict__))

    def from_disk(self, path, **kwargs):
        with open(path, 'rb') as handle:
            self.__dict__.update(pickle.loads(handle.read()))


Language.factories['tokenizer'] = WhitespaceTokenizer

####################
# HYPER PARAMETERS #
####################

model = None
new_model_name = "bio_tagging_spacy"
output_dir = '../spacy_out'
n_iter = 35
    

"""Set up the pipeline and entity recognizer, and train the new entity."""
random.seed(0)
if model is not None:
    nlp = spacy.load(model)  # load existing spaCy model
    print("Loaded model '%s'" % model)
else:
    nlp = spacy.blank("en")  # create blank Language class
    print("Created blank 'en' model")

# Add entity recognizer to model if it's not in the pipeline
# nlp.create_pipe works for built-ins that are registered with spaCy
if "ner" not in nlp.pipe_names:
    ner = nlp.create_pipe("ner")
    nlp.add_pipe(ner)
# otherwise, get it, so we can add labels to it
else:
    ner = nlp.get_pipe("ner")

ner.add_label(B)  # add new entity label to entity recognizer
ner.add_label(I)
ner.add_label(O)

# Adding extraneous labels shouldn't mess anything up
# ner.add_label("VEGETABLE")
if model is None:
    optimizer = nlp.begin_training()
else:
    optimizer = nlp.resume_training()
move_names = list(ner.move_names)

# get names of other pipes to disable them during training
pipe_exceptions = ["ner", "trf_wordpiecer", "trf_tok2vec"]
other_pipes = [pipe for pipe in nlp.pipe_names if pipe not in pipe_exceptions]

# only train NER
with nlp.disable_pipes(*other_pipes), warnings.catch_warnings():
    # show warnings for misaligned entity spans once
    warnings.filterwarnings("once", category=UserWarning, module='spacy')

    sizes = compounding(1.0, 4.0, 1.001)
    # batch up the examples using spaCy's minibatch
    for itn in range(n_iter):
        random.shuffle(TRAIN_DATA)
        batches = minibatch(TRAIN_DATA, size=sizes)
        losses = {}
        for batch in batches:
            texts, annotations = zip(*batch)
            nlp.update(texts, annotations, sgd=optimizer, drop=0.35, losses=losses)
        print("Losses", losses)


# test the trained model
total_predicted_label = []
total_expected_label = []
nlp.tokenizer = WhitespaceTokenizer(nlp.vocab)

for i in range(len(test_sentences)):
    doc = nlp(test_sentences[i])
    predicted_label = []

    for ent in doc.ents:
        predicted_label.append(ent.label_)

    assert len(predicted_label) == len(test_tag[i])
    total_expected_label.extend(test_tag[i])
    total_predicted_label.extend(predicted_label)

assert len(total_expected_label) == len(total_predicted_label)

target_names = ['class B', 'class I', 'class O']
print(classification_report(total_expected_label, total_predicted_label, target_names=target_names))


def create_tokenizer(nlp):
    return WhitespaceTokenizer(nlp)  # or however you custom tokenizer is initialised


# save model to output directory
if output_dir is not None:
    output_dir = Path(output_dir)
    if not output_dir.exists():
        output_dir.mkdir()
    nlp.meta["name"] = new_model_name  # rename model
    nlp.to_disk(output_dir)
    print("Saved model to", output_dir)

    # # test the saved model
    
    Language.Defaults.create_tokenizer = WhitespaceTokenizer
    print("Loading from", output_dir)
    nlp2 = spacy.load(output_dir)
    
    # # Check the classes have loaded back consistently
    assert nlp2.get_pipe("ner").move_names == move_names
    doc2 = nlp2(test_sentences[i])
    for ent in doc2.ents:
        print(ent.label_, ent.text)


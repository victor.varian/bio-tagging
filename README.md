## Getting Started
Slides: https://docs.google.com/presentation/d/1HfvYuQaP52YLh_f_qfr3e3DssijTKV73FqAiVF6LKEw/edit#slide=id.gbfdddaba3c_1_56


Start your python environment:
Ideally: `python3 -m venv env && source env/bin/activate`

Things to do before run the scripts:
1. `pip install -r requirements.txt`
2. `export TOKENIZERS_PARALLELISM=false`
3. Make sure you have your model saved in `/input` folder or somewhere from the internet (example: transformers pretrained model). Make some changes in `config.py` corresponding to your model details. You may want to go to `model.py` file and modify `self.bert` if neccessary.

<br>

### Custom NER model

**Start the training:**
```
cd src
python3 train.py
```

**Start predicting:**

Predicting a csv file:
```
python3 predict_csv.py
```

This will output `predict_file.csv` as the final result

<br>

Predicting only a sentence (to test the model yourself):
```
python3 predict_sentence.py
```
Usage: type your string in the cmd and it will return the corresponding entity. To exit, press enter twice.

<br>

### Spacy NER
Train the model:
```
python3 tagger.py
```

Predicting only a sentence (to test the model yourself):
```
python3 predict.py
```

Additional Comment:
The train data and test data that is used on spacy dataset is located at `input/` folder (it is the same as the custom NER)

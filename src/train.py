import pandas as pd
import numpy as np

import joblib
import torch

from sklearn import preprocessing
from sklearn import model_selection

from transformers import AdamW
from transformers import get_linear_schedule_with_warmup

import config
import dataset
import engine
from model import EntityModel


def process_data(data_path):
    df = pd.read_csv(data_path, header=None, encoding="latin-1")
    df = df[:202]
    max_len = len(df.columns)
    
    texts = df[::2]
    texts.reset_index(inplace=True)
    del texts['index']
    
    labels = df[1::2]
    labels.reset_index(inplace=True)
    del labels['index']

    sentences = []
    for i in range(len(texts)):
        counter = 0
        tmp = []
        
        while counter < max_len and (not pd.isna(texts.loc[i][counter])):
            tmp.append(texts.loc[i][counter])
            counter += 1
        
        sentences.append(tmp)

    tmp_tag = []
    for i in range(len(labels)):
        counter = 0
        tmp = []
        
        while counter < max_len and (not pd.isna(labels.loc[i][counter])):
            tmp.append(labels.loc[i][counter])
            counter += 1
            
        tmp_tag.append(tmp)

    sentences = np.array(sentences)
    tag = []
    enc_tag = preprocessing.LabelEncoder()
    enc_tag.fit(["B", "I", "O"])

    for i in range(len(tmp_tag)):
        tag.append(list(enc_tag.transform(tmp_tag[i])))
    
    tag = np.array(tag)
    return sentences, tag, enc_tag


if __name__ == "__main__":
    sentences, tag, enc_tag = process_data(config.TRAINING_FILE)
    meta_data = {
        "enc_tag": enc_tag
    }

    joblib.dump(meta_data, "meta.bin")

    num_tag = len(list(enc_tag.classes_))

    (
        train_sentences,
        test_sentences,
        train_tag,
        test_tag
    ) = model_selection.train_test_split(sentences, tag, random_state=42, shuffle=True, test_size=0.15)

    train_dataset = dataset.EntityDataset(
        texts=train_sentences, tags=train_tag
    )

    train_data_loader = torch.utils.data.DataLoader(
        train_dataset, batch_size=config.TRAIN_BATCH_SIZE, num_workers=4
    )

    valid_dataset = dataset.EntityDataset(
        texts=test_sentences, tags=test_tag
    )

    valid_data_loader = torch.utils.data.DataLoader(
        valid_dataset, batch_size=config.VALID_BATCH_SIZE, num_workers=1
    )

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    model = EntityModel(num_tag=num_tag)
    model.to(device)

    param_optimizer = list(model.named_parameters())
    no_decay = ["bias", "LayerNorm.bias", "LayerNorm.weight"]
    optimizer_parameters = [
        {
            "params": [
                p for n, p in param_optimizer if not any(nd in n for nd in no_decay)
            ],
            "weight_decay": 0.001,
        },
        {
            "params": [
                p for n, p in param_optimizer if any(nd in n for nd in no_decay)
            ],
            "weight_decay": 0.0,
        },
    ]

    num_train_steps = int(len(train_sentences) / config.TRAIN_BATCH_SIZE * config.EPOCHS)
    optimizer = AdamW(optimizer_parameters, lr=3e-5)
    scheduler = get_linear_schedule_with_warmup(
        optimizer, num_warmup_steps=0, num_training_steps=num_train_steps
    )

    best_loss = np.inf
    increased = False
    num_increased = 0

    for epoch in range(config.EPOCHS):
        train_loss = engine.train_fn(train_data_loader, model, optimizer, device, scheduler)
        test_loss = engine.eval_fn(valid_data_loader, model, device)
        print(f"Train Loss = {train_loss} Valid Loss = {test_loss}")

        if test_loss < best_loss:
            torch.save(model.state_dict(), config.MODEL_PATH)
            best_loss = test_loss
            increased = False
        else:
            if (not increased):
                increased = True
            else: # if model doesn't improve in two consecutive iteration, break
                break

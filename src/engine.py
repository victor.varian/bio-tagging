import torch
import numpy as np
from tqdm import tqdm
from sklearn.metrics import classification_report

import time

def print_f1_score(data, tag, target_tag):
    predicted_tag = tag.argmax(dim=2)

    final_predict = []
    final_target = []

    for i in range(len(predicted_tag)):
        matrix = {}
        final_f1_B = 0
        final_f1_I = 0
        final_f1_O = 0
        matrix[0] = {0: 0, 1: 0, 2: 0}
        matrix[1] = {0: 0, 1: 0, 2: 0}
        matrix[2] = {0: 0, 1: 0, 2: 0}
        max_len = 0

        while (data['ids'][i][max_len] != 1):
            max_len += 1
        
        init_predict = predicted_tag[i][:max_len]
        init_target = target_tag[i][:max_len]

        tmp_predict = np.array([], dtype=int)
        tmp_target = np.array([], dtype=int)

        for i in range(max_len):
            if (init_target[i] == -100):
                continue

            tmp_predict = np.append(tmp_predict, int(init_predict[i]))
            tmp_target = np.append(tmp_target, int(init_target[i]))

        tmp_predict = torch.from_numpy(tmp_predict)
        tmp_target = torch.from_numpy(tmp_target)

        final_predict.extend(tmp_predict)
        final_target.extend(tmp_target)

        for j in range(len(tmp_predict)):
            pred_idx = int(tmp_predict[j])
            target_idx = int(tmp_target[j])
            matrix[pred_idx][target_idx] += 1
        
        precision_B, precision_I, precision_O = None, None, None
        recall_B, recall_I, recall_O = None, None, None

        if (sum(matrix[0].values()) != 0):
            precision_B = matrix[0][0] / sum(matrix[0].values())

        if (sum(matrix[1].values()) != 0):
            precision_I = matrix[1][1] / sum(matrix[1].values())

        if (sum(matrix[2].values()) != 0):
            precision_O = matrix[2][2] / sum(matrix[2].values())

        if (matrix[0][0] + matrix[1][0] + matrix[2][0] != 0):
            recall_B = matrix[0][0] / (matrix[0][0] + matrix[1][0] + matrix[2][0])
        
        if (matrix[0][1] + matrix[1][1] + matrix[2][1] != 0):
            recall_I = matrix[1][1] / (matrix[0][1] + matrix[1][1] + matrix[2][1])

        if (matrix[0][2] + matrix[1][2] + matrix[2][2] != 0):
            recall_O = matrix[2][2] / (matrix[0][2] + matrix[1][2] + matrix[2][2])
        
        if (precision_B != None and recall_B != None) and (precision_B + recall_B != 0):
            final_f1_B = (2 * precision_B * recall_B) / (precision_B + recall_B)
        else:
            print("Missing B, Actual number of B: ", matrix[0][0] + matrix[1][0] + matrix[2][0])

        if (precision_I != None and recall_I != None) and (precision_I + recall_I != 0):
            final_f1_I = (2 * precision_I * recall_I) / (precision_I + recall_I)
        else:
            print("Missing I, Actual number of I: ", matrix[0][1] + matrix[1][1] + matrix[2][1])

        if (precision_O != None and recall_O != None) and (precision_O + recall_O != 0):
            final_f1_O = (2 * precision_O * recall_O) / (precision_O + recall_O)
        else:
            print("Missing O, Actual number of O: ", matrix[0][2] + matrix[1][2] + matrix[2][2])

        print("F1 SCORE of B:", round(final_f1_B, 3), "| I:", round(final_f1_I, 3), "| O:", round(final_f1_O, 3))

    return final_predict, final_target


def train_fn(data_loader, model, optimizer, device, scheduler):
    model.train()
    final_loss = 0
    
    for data in tqdm(data_loader, total=len(data_loader)):
        for k, v in data.items():
            data[k] = v.to(device)
        optimizer.zero_grad()
        tag, loss, target_tag = model(**data)
        loss.backward()
        optimizer.step()
        scheduler.step()
        # print_f1_score(data, tag, target_tag)
        final_loss += loss.item()
        
    return final_loss / len(data_loader)


def eval_fn(data_loader, model, device):
    model.eval()
    final_loss = 0
    final_predict = []
    final_target = []

    for data in tqdm(data_loader, total=len(data_loader)):
        for k, v in data.items():
            data[k] = v.to(device)
        tag, loss, target_tag = model(**data)
        predict, target = print_f1_score(data, tag, target_tag)
        final_predict.extend(predict)
        final_target.extend(target)
        final_loss += loss.item()

    
    target_names = ['B', 'I', 'O']
    print(classification_report(final_target, final_predict, target_names=target_names))

    return final_loss / len(data_loader)

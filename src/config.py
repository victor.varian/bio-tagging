import transformers

MAX_LEN = 128
TRAIN_BATCH_SIZE = 8
VALID_BATCH_SIZE = 4
EPOCHS = 20
MODEL_PATH = "model.bin"
TRAINING_FILE = "../input/tag_dataset.csv"
BASE_MODEL_PATH = "../input/shopee-roberta-small"
# BASE_MODEL_PATH = "roberta-base"

TOKENIZER = transformers.AutoTokenizer.from_pretrained(
    BASE_MODEL_PATH,
    do_lower_case=True
)

import numpy as np
import pandas as pd

import joblib
import torch
import csv

import config
import dataset
import engine
from model import EntityModel


if __name__ == "__main__":

    meta_data = joblib.load("meta.bin")
    enc_tag = meta_data["enc_tag"]
    num_tag = len(list(enc_tag.classes_))

    df = pd.read_csv(config.TRAINING_FILE, header=None, encoding="latin-1")
    df = df[202:]
    max_len = len(df.columns)
    
    texts = df[::2]
    texts.reset_index(inplace=True)
    del texts['index']

    sentences = []
    for i in range(len(texts)):
        counter = 0
        tmp = []
        
        while counter < max_len and (not pd.isna(texts.loc[i][counter])):
            tmp.append(texts.loc[i][counter])
            counter += 1
        
        sentences.append(tmp)

    sentences = list(map(lambda x: " ".join(x), sentences))
    with open('predict_file.csv', mode='w') as predict_file:
        predict_writer = csv.writer(predict_file, delimiter=',')

        for stx in sentences:
            tokenized_sentence = config.TOKENIZER.encode(stx)
            sentence = stx.split()

            test_dataset = dataset.EntityDataset(
                texts=[sentence],
                tags=[[0] * len(sentence)]
            )

            device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
            model = EntityModel(num_tag=num_tag)
            model.load_state_dict(torch.load(config.MODEL_PATH))
            model.to(device)

            result = None
            with torch.no_grad():
                data = test_dataset[0]
                for k, v in data.items():
                    data[k] = v.to(device).unsqueeze(0)
                tag, _, _ = model(**data)

                result = (
                    enc_tag.inverse_transform(
                        tag.argmax(2).cpu().numpy().reshape(-1)
                    )[:len(tokenized_sentence)]
                )

            print(tokenized_sentence)
            id_to_tokens = config.TOKENIZER.convert_ids_to_tokens(tokenized_sentence)
            predict_writer.writerow(sentence)
            predict_writer.writerow(id_to_tokens)
            predict_writer.writerow(tokenized_sentence)
            predict_writer.writerow(result)
